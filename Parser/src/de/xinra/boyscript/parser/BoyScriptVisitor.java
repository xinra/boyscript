// Generated from ./grammar/BoyScript.g4 by ANTLR 4.5
package de.xinra.boyscript.parser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BoyScriptParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BoyScriptVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(BoyScriptParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#programPart}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgramPart(BoyScriptParser.ProgramPartContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#comment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComment(BoyScriptParser.CommentContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#leftBracket}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLeftBracket(BoyScriptParser.LeftBracketContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#rightBracket}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRightBracket(BoyScriptParser.RightBracketContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#curlyBracket}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCurlyBracket(BoyScriptParser.CurlyBracketContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(BoyScriptParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#condIf}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondIf(BoyScriptParser.CondIfContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#condElse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondElse(BoyScriptParser.CondElseContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop(BoyScriptParser.LoopContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(BoyScriptParser.ConditionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Echo}
	 * labeled alternative in {@link BoyScriptParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEcho(BoyScriptParser.EchoContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcCall}
	 * labeled alternative in {@link BoyScriptParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncCall(BoyScriptParser.FuncCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Assignment}
	 * labeled alternative in {@link BoyScriptParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(BoyScriptParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Include}
	 * labeled alternative in {@link BoyScriptParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInclude(BoyScriptParser.IncludeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Alias}
	 * labeled alternative in {@link BoyScriptParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlias(BoyScriptParser.AliasContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Return}
	 * labeled alternative in {@link BoyScriptParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn(BoyScriptParser.ReturnContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Val}
	 * labeled alternative in {@link BoyScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVal(BoyScriptParser.ValContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Brackets}
	 * labeled alternative in {@link BoyScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBrackets(BoyScriptParser.BracketsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RecursiveExpression}
	 * labeled alternative in {@link BoyScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecursiveExpression(BoyScriptParser.RecursiveExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#expressionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionList(BoyScriptParser.ExpressionListContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#functionCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall(BoyScriptParser.FunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#variableList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableList(BoyScriptParser.VariableListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Literal}
	 * labeled alternative in {@link BoyScriptParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(BoyScriptParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Var}
	 * labeled alternative in {@link BoyScriptParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar(BoyScriptParser.VarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code False}
	 * labeled alternative in {@link BoyScriptParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFalse(BoyScriptParser.FalseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code True}
	 * labeled alternative in {@link BoyScriptParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrue(BoyScriptParser.TrueContext ctx);
	/**
	 * Visit a parse tree produced by {@link BoyScriptParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(BoyScriptParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Add}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(BoyScriptParser.AddContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Diff}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiff(BoyScriptParser.DiffContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Mult}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMult(BoyScriptParser.MultContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Div}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiv(BoyScriptParser.DivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Mod}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMod(BoyScriptParser.ModContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Power}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPower(BoyScriptParser.PowerContext ctx);
	/**
	 * Visit a parse tree produced by the {@code And}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(BoyScriptParser.AndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Or}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(BoyScriptParser.OrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Concatenation}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcatenation(BoyScriptParser.ConcatenationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Equal}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqual(BoyScriptParser.EqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Identical}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentical(BoyScriptParser.IdenticalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NotEqual}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotEqual(BoyScriptParser.NotEqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Smaller}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSmaller(BoyScriptParser.SmallerContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Greater}
	 * labeled alternative in {@link BoyScriptParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGreater(BoyScriptParser.GreaterContext ctx);
}
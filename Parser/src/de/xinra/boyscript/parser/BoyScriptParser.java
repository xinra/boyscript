// Generated from ./grammar/BoyScript.g4 by ANTLR 4.5
package de.xinra.boyscript.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BoyScriptParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, ML_COMMENT=42, SL_COMMENT=43, STRING=44, 
		ECHO=45, ASSIGN=46, INCLUDE=47, FALSE=48, TRUE=49, ID=50, NUMBER=51, WHITESPACE=52;
	public static final int
		RULE_program = 0, RULE_programPart = 1, RULE_comment = 2, RULE_leftBracket = 3, 
		RULE_rightBracket = 4, RULE_curlyBracket = 5, RULE_function = 6, RULE_condIf = 7, 
		RULE_condElse = 8, RULE_loop = 9, RULE_condition = 10, RULE_statement = 11, 
		RULE_expression = 12, RULE_expressionList = 13, RULE_functionCall = 14, 
		RULE_variableList = 15, RULE_value = 16, RULE_variable = 17, RULE_operator = 18;
	public static final String[] ruleNames = {
		"program", "programPart", "comment", "leftBracket", "rightBracket", "curlyBracket", 
		"function", "condIf", "condElse", "loop", "condition", "statement", "expression", 
		"expressionList", "functionCall", "variableList", "value", "variable", 
		"operator"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'Klammer auf '", "' Klammer zu'", "'sheesh'", "'der Boy will '", 
		"' am '", "'am '", "' sein'", "'wenn '", "'ansonsten'", "'solange'", "' real ist'", 
		"' ist'", "' sind'", "' real sind'", "'der Boy ist'", "'swagify '", "' rausballen'", 
		"' sowie '", "'der '", "'die '", "'das '", "'den '", "'dem'", "'eine '", 
		"'einen'", "'ein '", "' an der Stelle'", "' plus '", "' minus '", "' mal '", 
		"' durch '", "' geteilt mit Rest durch '", "' hoch '", "' und '", "' oder '", 
		"' zusammengeballt mit '", "' gleich '", "' identisch mit'", "' ungleich '", 
		"' kleiner als '", "' größer als '", null, null, null, "' amk'", "' ist '", 
		"' reinballen'", "'nicht real'", "'real'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, "ML_COMMENT", "SL_COMMENT", "STRING", 
		"ECHO", "ASSIGN", "INCLUDE", "FALSE", "TRUE", "ID", "NUMBER", "WHITESPACE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BoyScript.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BoyScriptParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public List<ProgramPartContext> programPart() {
			return getRuleContexts(ProgramPartContext.class);
		}
		public ProgramPartContext programPart(int i) {
			return getRuleContext(ProgramPartContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(39); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(38);
				programPart();
				}
				}
				setState(41); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__2) | (1L << T__3) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__14) | (1L << T__15) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << ML_COMMENT) | (1L << SL_COMMENT) | (1L << STRING) | (1L << FALSE) | (1L << TRUE) | (1L << NUMBER))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramPartContext extends ParserRuleContext {
		public CommentContext comment() {
			return getRuleContext(CommentContext.class,0);
		}
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public CondIfContext condIf() {
			return getRuleContext(CondIfContext.class,0);
		}
		public CondElseContext condElse() {
			return getRuleContext(CondElseContext.class,0);
		}
		public LoopContext loop() {
			return getRuleContext(LoopContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public CurlyBracketContext curlyBracket() {
			return getRuleContext(CurlyBracketContext.class,0);
		}
		public ProgramPartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_programPart; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitProgramPart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramPartContext programPart() throws RecognitionException {
		ProgramPartContext _localctx = new ProgramPartContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_programPart);
		try {
			setState(50);
			switch (_input.LA(1)) {
			case ML_COMMENT:
			case SL_COMMENT:
				enterOuterAlt(_localctx, 1);
				{
				setState(43);
				comment();
				}
				break;
			case T__3:
				enterOuterAlt(_localctx, 2);
				{
				setState(44);
				function();
				}
				break;
			case T__7:
				enterOuterAlt(_localctx, 3);
				{
				setState(45);
				condIf();
				}
				break;
			case T__8:
				enterOuterAlt(_localctx, 4);
				{
				setState(46);
				condElse();
				}
				break;
			case T__9:
				enterOuterAlt(_localctx, 5);
				{
				setState(47);
				loop();
				}
				break;
			case T__0:
			case T__14:
			case T__15:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
			case T__24:
			case T__25:
			case STRING:
			case FALSE:
			case TRUE:
			case NUMBER:
				enterOuterAlt(_localctx, 6);
				{
				setState(48);
				statement();
				}
				break;
			case T__2:
				enterOuterAlt(_localctx, 7);
				{
				setState(49);
				curlyBracket();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommentContext extends ParserRuleContext {
		public TerminalNode ML_COMMENT() { return getToken(BoyScriptParser.ML_COMMENT, 0); }
		public TerminalNode SL_COMMENT() { return getToken(BoyScriptParser.SL_COMMENT, 0); }
		public CommentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comment; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitComment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommentContext comment() throws RecognitionException {
		CommentContext _localctx = new CommentContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_comment);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(52);
			_la = _input.LA(1);
			if ( !(_la==ML_COMMENT || _la==SL_COMMENT) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LeftBracketContext extends ParserRuleContext {
		public LeftBracketContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_leftBracket; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitLeftBracket(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LeftBracketContext leftBracket() throws RecognitionException {
		LeftBracketContext _localctx = new LeftBracketContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_leftBracket);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(54);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RightBracketContext extends ParserRuleContext {
		public RightBracketContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rightBracket; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitRightBracket(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RightBracketContext rightBracket() throws RecognitionException {
		RightBracketContext _localctx = new RightBracketContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_rightBracket);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CurlyBracketContext extends ParserRuleContext {
		public CurlyBracketContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_curlyBracket; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitCurlyBracket(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CurlyBracketContext curlyBracket() throws RecognitionException {
		CurlyBracketContext _localctx = new CurlyBracketContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_curlyBracket);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public VariableListContext params;
		public Token name;
		public TerminalNode ID() { return getToken(BoyScriptParser.ID, 0); }
		public VariableListContext variableList() {
			return getRuleContext(VariableListContext.class,0);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			match(T__3);
			setState(62);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25))) != 0)) {
				{
				setState(61);
				((FunctionContext)_localctx).params = variableList();
				}
			}

			setState(64);
			_la = _input.LA(1);
			if ( !(_la==T__4 || _la==T__5) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(65);
			((FunctionContext)_localctx).name = match(ID);
			setState(66);
			match(T__6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CondIfContext extends ParserRuleContext {
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public CondIfContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condIf; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitCondIf(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CondIfContext condIf() throws RecognitionException {
		CondIfContext _localctx = new CondIfContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_condIf);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68);
			match(T__7);
			setState(69);
			condition();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CondElseContext extends ParserRuleContext {
		public CondElseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condElse; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitCondElse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CondElseContext condElse() throws RecognitionException {
		CondElseContext _localctx = new CondElseContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_condElse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			match(T__8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LoopContext extends ParserRuleContext {
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public LoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitLoop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LoopContext loop() throws RecognitionException {
		LoopContext _localctx = new LoopContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_loop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			match(T__9);
			setState(74);
			condition();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			expression(0);
			setState(77);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	 
		public StatementContext() { }
		public void copyFrom(StatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AliasContext extends StatementContext {
		public Token original;
		public Token alias;
		public List<TerminalNode> ID() { return getTokens(BoyScriptParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(BoyScriptParser.ID, i);
		}
		public AliasContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitAlias(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EchoContext extends StatementContext {
		public ExpressionContext output;
		public TerminalNode ECHO() { return getToken(BoyScriptParser.ECHO, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public EchoContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitEcho(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ReturnContext extends StatementContext {
		public ExpressionContext returnValue;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ReturnContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitReturn(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AssignmentContext extends StatementContext {
		public VariableContext varName;
		public TerminalNode ASSIGN() { return getToken(BoyScriptParser.ASSIGN, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AssignmentContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitAssignment(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IncludeContext extends StatementContext {
		public ExpressionContext file;
		public TerminalNode INCLUDE() { return getToken(BoyScriptParser.INCLUDE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public IncludeContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitInclude(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FuncCallContext extends StatementContext {
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public FuncCallContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitFuncCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_statement);
		try {
			setState(99);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				_localctx = new EchoContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(79);
				((EchoContext)_localctx).output = expression(0);
				setState(80);
				match(ECHO);
				}
				break;
			case 2:
				_localctx = new FuncCallContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(82);
				match(T__14);
				setState(83);
				functionCall();
				}
				break;
			case 3:
				_localctx = new AssignmentContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(84);
				((AssignmentContext)_localctx).varName = variable();
				setState(85);
				match(ASSIGN);
				setState(88);
				switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
				case 1:
					{
					setState(86);
					functionCall();
					}
					break;
				case 2:
					{
					setState(87);
					expression(0);
					}
					break;
				}
				}
				break;
			case 4:
				_localctx = new IncludeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(90);
				((IncludeContext)_localctx).file = expression(0);
				setState(91);
				match(INCLUDE);
				}
				break;
			case 5:
				_localctx = new AliasContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(93);
				match(T__15);
				setState(94);
				((AliasContext)_localctx).original = match(ID);
				setState(95);
				((AliasContext)_localctx).alias = match(ID);
				}
				break;
			case 6:
				_localctx = new ReturnContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(96);
				((ReturnContext)_localctx).returnValue = expression(0);
				setState(97);
				match(T__16);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BracketsContext extends ExpressionContext {
		public LeftBracketContext leftBracket() {
			return getRuleContext(LeftBracketContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RightBracketContext rightBracket() {
			return getRuleContext(RightBracketContext.class,0);
		}
		public BracketsContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitBrackets(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ValContext extends ExpressionContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ValContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RecursiveExpressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public OperatorContext operator() {
			return getRuleContext(OperatorContext.class,0);
		}
		public RecursiveExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitRecursiveExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 24;
		enterRecursionRule(_localctx, 24, RULE_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			switch (_input.LA(1)) {
			case T__0:
				{
				_localctx = new BracketsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(102);
				leftBracket();
				setState(103);
				expression(0);
				setState(104);
				rightBracket();
				}
				break;
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
			case T__24:
			case T__25:
			case STRING:
			case FALSE:
			case TRUE:
			case NUMBER:
				{
				_localctx = new ValContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(106);
				value();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(115);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new RecursiveExpressionContext(new ExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_expression);
					setState(109);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(110);
					operator();
					setState(111);
					expression(3);
					}
					} 
				}
				setState(117);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExpressionListContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public ExpressionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitExpressionList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionListContext expressionList() throws RecognitionException {
		ExpressionListContext _localctx = new ExpressionListContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_expressionList);
		try {
			setState(123);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(118);
				expression(0);
				setState(119);
				match(T__17);
				setState(120);
				expressionList();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(122);
				expression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public ExpressionListContext params;
		public Token functionName;
		public TerminalNode ID() { return getToken(BoyScriptParser.ID, 0); }
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitFunctionCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_functionCall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(126);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << STRING) | (1L << FALSE) | (1L << TRUE) | (1L << NUMBER))) != 0)) {
				{
				setState(125);
				((FunctionCallContext)_localctx).params = expressionList();
				}
			}

			setState(128);
			_la = _input.LA(1);
			if ( !(_la==T__4 || _la==T__5) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(129);
			((FunctionCallContext)_localctx).functionName = match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableListContext extends ParserRuleContext {
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public VariableListContext variableList() {
			return getRuleContext(VariableListContext.class,0);
		}
		public VariableListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitVariableList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableListContext variableList() throws RecognitionException {
		VariableListContext _localctx = new VariableListContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_variableList);
		try {
			setState(136);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(131);
				variable();
				setState(132);
				match(T__17);
				setState(133);
				variableList();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(135);
				variable();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
	 
		public ValueContext() { }
		public void copyFrom(ValueContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LiteralContext extends ValueContext {
		public TerminalNode STRING() { return getToken(BoyScriptParser.STRING, 0); }
		public TerminalNode NUMBER() { return getToken(BoyScriptParser.NUMBER, 0); }
		public LiteralContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TrueContext extends ValueContext {
		public TerminalNode TRUE() { return getToken(BoyScriptParser.TRUE, 0); }
		public TrueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitTrue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarContext extends ValueContext {
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public VarContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitVar(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FalseContext extends ValueContext {
		public TerminalNode FALSE() { return getToken(BoyScriptParser.FALSE, 0); }
		public FalseContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitFalse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_value);
		try {
			setState(143);
			switch (_input.LA(1)) {
			case STRING:
				_localctx = new LiteralContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(138);
				match(STRING);
				}
				break;
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
			case T__24:
			case T__25:
				_localctx = new VarContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(139);
				variable();
				}
				break;
			case NUMBER:
				_localctx = new LiteralContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(140);
				match(NUMBER);
				}
				break;
			case FALSE:
				_localctx = new FalseContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(141);
				match(FALSE);
				}
				break;
			case TRUE:
				_localctx = new TrueContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(142);
				match(TRUE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public Token name;
		public ExpressionContext index;
		public TerminalNode ID() { return getToken(BoyScriptParser.ID, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_variable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(145);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(146);
			((VariableContext)_localctx).name = match(ID);
			setState(149);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				setState(147);
				match(T__26);
				setState(148);
				((VariableContext)_localctx).index = expression(0);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperatorContext extends ParserRuleContext {
		public OperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operator; }
	 
		public OperatorContext() { }
		public void copyFrom(OperatorContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class MultContext extends OperatorContext {
		public MultContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitMult(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdenticalContext extends OperatorContext {
		public IdenticalContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitIdentical(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConcatenationContext extends OperatorContext {
		public ConcatenationContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitConcatenation(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ModContext extends OperatorContext {
		public ModContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitMod(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotEqualContext extends OperatorContext {
		public NotEqualContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitNotEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PowerContext extends OperatorContext {
		public PowerContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitPower(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OrContext extends OperatorContext {
		public OrContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqualContext extends OperatorContext {
		public EqualContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DiffContext extends OperatorContext {
		public DiffContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitDiff(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AddContext extends OperatorContext {
		public AddContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitAdd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GreaterContext extends OperatorContext {
		public GreaterContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitGreater(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SmallerContext extends OperatorContext {
		public SmallerContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitSmaller(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndContext extends OperatorContext {
		public AndContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitAnd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DivContext extends OperatorContext {
		public DivContext(OperatorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BoyScriptVisitor ) return ((BoyScriptVisitor<? extends T>)visitor).visitDiv(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperatorContext operator() throws RecognitionException {
		OperatorContext _localctx = new OperatorContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_operator);
		try {
			setState(165);
			switch (_input.LA(1)) {
			case T__27:
				_localctx = new AddContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(151);
				match(T__27);
				}
				break;
			case T__28:
				_localctx = new DiffContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(152);
				match(T__28);
				}
				break;
			case T__29:
				_localctx = new MultContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(153);
				match(T__29);
				}
				break;
			case T__30:
				_localctx = new DivContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(154);
				match(T__30);
				}
				break;
			case T__31:
				_localctx = new ModContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(155);
				match(T__31);
				}
				break;
			case T__32:
				_localctx = new PowerContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(156);
				match(T__32);
				}
				break;
			case T__33:
				_localctx = new AndContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(157);
				match(T__33);
				}
				break;
			case T__34:
				_localctx = new OrContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(158);
				match(T__34);
				}
				break;
			case T__35:
				_localctx = new ConcatenationContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(159);
				match(T__35);
				}
				break;
			case T__36:
				_localctx = new EqualContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(160);
				match(T__36);
				}
				break;
			case T__37:
				_localctx = new IdenticalContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(161);
				match(T__37);
				}
				break;
			case T__38:
				_localctx = new NotEqualContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(162);
				match(T__38);
				}
				break;
			case T__39:
				_localctx = new SmallerContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(163);
				match(T__39);
				}
				break;
			case T__40:
				_localctx = new GreaterContext(_localctx);
				enterOuterAlt(_localctx, 14);
				{
				setState(164);
				match(T__40);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 12:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\66\u00aa\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\3\2\6\2*\n\2\r\2\16\2+\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\5\3\65\n\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\5\bA\n\b\3\b\3\b"+
		"\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\5\r[\n\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5"+
		"\rf\n\r\3\16\3\16\3\16\3\16\3\16\3\16\5\16n\n\16\3\16\3\16\3\16\3\16\7"+
		"\16t\n\16\f\16\16\16w\13\16\3\17\3\17\3\17\3\17\3\17\5\17~\n\17\3\20\5"+
		"\20\u0081\n\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\5\21\u008b\n\21"+
		"\3\22\3\22\3\22\3\22\3\22\5\22\u0092\n\22\3\23\3\23\3\23\3\23\5\23\u0098"+
		"\n\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\5\24\u00a8\n\24\3\24\2\3\32\25\2\4\6\b\n\f\16\20\22\24\26\30\32"+
		"\34\36 \"$&\2\6\3\2,-\3\2\7\b\3\2\r\20\3\2\25\34\u00bb\2)\3\2\2\2\4\64"+
		"\3\2\2\2\6\66\3\2\2\2\b8\3\2\2\2\n:\3\2\2\2\f<\3\2\2\2\16>\3\2\2\2\20"+
		"F\3\2\2\2\22I\3\2\2\2\24K\3\2\2\2\26N\3\2\2\2\30e\3\2\2\2\32m\3\2\2\2"+
		"\34}\3\2\2\2\36\u0080\3\2\2\2 \u008a\3\2\2\2\"\u0091\3\2\2\2$\u0093\3"+
		"\2\2\2&\u00a7\3\2\2\2(*\5\4\3\2)(\3\2\2\2*+\3\2\2\2+)\3\2\2\2+,\3\2\2"+
		"\2,\3\3\2\2\2-\65\5\6\4\2.\65\5\16\b\2/\65\5\20\t\2\60\65\5\22\n\2\61"+
		"\65\5\24\13\2\62\65\5\30\r\2\63\65\5\f\7\2\64-\3\2\2\2\64.\3\2\2\2\64"+
		"/\3\2\2\2\64\60\3\2\2\2\64\61\3\2\2\2\64\62\3\2\2\2\64\63\3\2\2\2\65\5"+
		"\3\2\2\2\66\67\t\2\2\2\67\7\3\2\2\289\7\3\2\29\t\3\2\2\2:;\7\4\2\2;\13"+
		"\3\2\2\2<=\7\5\2\2=\r\3\2\2\2>@\7\6\2\2?A\5 \21\2@?\3\2\2\2@A\3\2\2\2"+
		"AB\3\2\2\2BC\t\3\2\2CD\7\64\2\2DE\7\t\2\2E\17\3\2\2\2FG\7\n\2\2GH\5\26"+
		"\f\2H\21\3\2\2\2IJ\7\13\2\2J\23\3\2\2\2KL\7\f\2\2LM\5\26\f\2M\25\3\2\2"+
		"\2NO\5\32\16\2OP\t\4\2\2P\27\3\2\2\2QR\5\32\16\2RS\7/\2\2Sf\3\2\2\2TU"+
		"\7\21\2\2Uf\5\36\20\2VW\5$\23\2WZ\7\60\2\2X[\5\36\20\2Y[\5\32\16\2ZX\3"+
		"\2\2\2ZY\3\2\2\2[f\3\2\2\2\\]\5\32\16\2]^\7\61\2\2^f\3\2\2\2_`\7\22\2"+
		"\2`a\7\64\2\2af\7\64\2\2bc\5\32\16\2cd\7\23\2\2df\3\2\2\2eQ\3\2\2\2eT"+
		"\3\2\2\2eV\3\2\2\2e\\\3\2\2\2e_\3\2\2\2eb\3\2\2\2f\31\3\2\2\2gh\b\16\1"+
		"\2hi\5\b\5\2ij\5\32\16\2jk\5\n\6\2kn\3\2\2\2ln\5\"\22\2mg\3\2\2\2ml\3"+
		"\2\2\2nu\3\2\2\2op\f\4\2\2pq\5&\24\2qr\5\32\16\5rt\3\2\2\2so\3\2\2\2t"+
		"w\3\2\2\2us\3\2\2\2uv\3\2\2\2v\33\3\2\2\2wu\3\2\2\2xy\5\32\16\2yz\7\24"+
		"\2\2z{\5\34\17\2{~\3\2\2\2|~\5\32\16\2}x\3\2\2\2}|\3\2\2\2~\35\3\2\2\2"+
		"\177\u0081\5\34\17\2\u0080\177\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0082"+
		"\3\2\2\2\u0082\u0083\t\3\2\2\u0083\u0084\7\64\2\2\u0084\37\3\2\2\2\u0085"+
		"\u0086\5$\23\2\u0086\u0087\7\24\2\2\u0087\u0088\5 \21\2\u0088\u008b\3"+
		"\2\2\2\u0089\u008b\5$\23\2\u008a\u0085\3\2\2\2\u008a\u0089\3\2\2\2\u008b"+
		"!\3\2\2\2\u008c\u0092\7.\2\2\u008d\u0092\5$\23\2\u008e\u0092\7\65\2\2"+
		"\u008f\u0092\7\62\2\2\u0090\u0092\7\63\2\2\u0091\u008c\3\2\2\2\u0091\u008d"+
		"\3\2\2\2\u0091\u008e\3\2\2\2\u0091\u008f\3\2\2\2\u0091\u0090\3\2\2\2\u0092"+
		"#\3\2\2\2\u0093\u0094\t\5\2\2\u0094\u0097\7\64\2\2\u0095\u0096\7\35\2"+
		"\2\u0096\u0098\5\32\16\2\u0097\u0095\3\2\2\2\u0097\u0098\3\2\2\2\u0098"+
		"%\3\2\2\2\u0099\u00a8\7\36\2\2\u009a\u00a8\7\37\2\2\u009b\u00a8\7 \2\2"+
		"\u009c\u00a8\7!\2\2\u009d\u00a8\7\"\2\2\u009e\u00a8\7#\2\2\u009f\u00a8"+
		"\7$\2\2\u00a0\u00a8\7%\2\2\u00a1\u00a8\7&\2\2\u00a2\u00a8\7\'\2\2\u00a3"+
		"\u00a8\7(\2\2\u00a4\u00a8\7)\2\2\u00a5\u00a8\7*\2\2\u00a6\u00a8\7+\2\2"+
		"\u00a7\u0099\3\2\2\2\u00a7\u009a\3\2\2\2\u00a7\u009b\3\2\2\2\u00a7\u009c"+
		"\3\2\2\2\u00a7\u009d\3\2\2\2\u00a7\u009e\3\2\2\2\u00a7\u009f\3\2\2\2\u00a7"+
		"\u00a0\3\2\2\2\u00a7\u00a1\3\2\2\2\u00a7\u00a2\3\2\2\2\u00a7\u00a3\3\2"+
		"\2\2\u00a7\u00a4\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a7\u00a6\3\2\2\2\u00a8"+
		"\'\3\2\2\2\17+\64@Zemu}\u0080\u008a\u0091\u0097\u00a7";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}
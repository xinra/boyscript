grammar BoyScript;

program: programPart+;

programPart
    : comment
    | function
    | condIf
    | condElse
    | loop
    | statement
    | curlyBracket
    ;

comment
    : ML_COMMENT
    | SL_COMMENT
    ;

leftBracket : 'Klammer auf ' ;
rightBracket : ' Klammer zu' ;
curlyBracket : 'sheesh' ;

function
    : 'der Boy will ' (params=variableList)? (' am '| 'am ') name=ID ' sein'
    ;

condIf
    : 'wenn ' condition
    ;

condElse
    : 'ansonsten'
    ;

loop
    : 'solange' condition
    ;

condition
    : expression (' real ist'|' ist'|' sind'|' real sind')
    ;

statement
    : output=expression ECHO #Echo
    | 'der Boy ist' functionCall #funcCall
    | varName=variable ASSIGN (functionCall|expression) #Assignment
    | file=expression INCLUDE #Include
    | 'swagify ' original=ID alias=ID #Alias
    | returnValue=expression ' rausballen' #Return
    ;

expression
    : leftBracket expression rightBracket #Brackets
    | expression operator expression #RecursiveExpression
    | value #Val
    ;

expressionList
    : expression ' sowie ' expressionList
    | expression
    ;

functionCall
    : params=expressionList? ('am '|' am ') functionName=ID
    ;

variableList
    : variable ' sowie ' variableList
    | variable
    ;

value
    : STRING #Literal
    | variable #Var
    | NUMBER #Literal
    | FALSE #False
    | TRUE #True
    ;

variable
    : ('der ' | 'die ' | 'das ' | 'den ' | 'dem' | 'eine ' | 'einen' | 'ein ') name=ID (' an der Stelle' index=expression)?
    ;

operator
    : ' plus ' #Add
    | ' minus ' #Diff
    | ' mal ' #Mult
    | ' durch ' #Div
    | ' geteilt mit Rest durch ' #Mod
    | ' hoch ' #Power
    | ' und ' #And
    | ' oder ' #Or
    | ' zusammengeballt mit ' #Concatenation
    | ' gleich ' #Equal
    | ' identisch mit' #Identical
    | ' ungleich ' #NotEqual
    | ' kleiner als ' #Smaller
    | ' größer als ' #Greater
    ;

ML_COMMENT
    : 'man halt die Fresse' .*? 'du Hurensohn'
    ;

SL_COMMENT
    : 'da machste nix' ~('\r' | '\n')*
    ;

STRING
    : '"' (STR_ESC | ~('\\' | '"' | '\r' | '\n'))* '"'
    ;

fragment STR_ESC
    : '\\' ('\\' | '"' | 't' | 'n' | 'r') // add more:  Unicode esapes, ...
    ;

ECHO: ' amk';
ASSIGN: ' ist ';
INCLUDE: ' reinballen';
FALSE: 'nicht real' ;
TRUE: 'real' ;

ID: [A-Za-z\_]+ [A-Za-z0-9\_]* ;

NUMBER: [0-9]+ ('.' [0-9]+)? ;

WHITESPACE: [ \t\n\r]+ -> skip ;
package de.xinra.boyscript.compiler;

import de.xinra.boyscript.parser.BoyScriptLexer;
import de.xinra.boyscript.parser.BoyScriptParser;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.PrintWriter;

/**
 * Erstellt von Erik Hofer am 12.05.2015.
 */
public class Compiler {
    public static void main(String[] args) throws Exception {
        if(args.length == 0 || args[0].equals("-help")) {
            System.out.println("Usage: java -jar boyscript.jar [-f] <file>\n" +
                    "\t-f creates .php file in the same directory\n" +
                    "Examples:\n" +
                    "\tjava -jar boyscript.jar test.bs > test.php\n" +
                    "\tjava -jar boyscript.jar -f *.bs");
            return;
        }

        boolean sysout = true;
        String filename = "";

        for(int i = 0; i < args.length; i++) {
            if(!args[i].startsWith("-")) {
                filename = args[i];
            } else if(args[i].equals("-f")) {
                sysout = false;
            }
        }

        String compiled = compile(new ANTLRFileStream(filename));

        if(sysout) {
            System.out.println(compiled);
        } else {
            //Only works if the file name has a postfix
            PrintWriter writer = new PrintWriter(filename.substring(0, filename.lastIndexOf('.'))+".php", "UTF-8");
            writer.print(compiled);
            writer.close();
        }
    }

    public static String compile(ANTLRInputStream input) {
        BoyScriptLexer lexer = new BoyScriptLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        BoyScriptParser parser = new BoyScriptParser(tokens);

        ParseTree tree = parser.program();
        return "<?php " + new PhpVisitor().visit(tree) + " ?>";
    }
}

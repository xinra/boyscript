package de.xinra.boyscript.compiler.test;

import de.xinra.boyscript.compiler.Compiler;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Erstellt von Erik Hofer am 12.05.2015.
 */
public class PhpTest {
    @SuppressWarnings({"unused"})
    @Test(dataProvider = "provide_code_expectedText")
    public void compiler_outputsExpectedText(String name, String code, String expectedText) {
        //execution
        String actualOutput = Compiler.compile(new ANTLRInputStream(code));

        //evaluation
        Assert.assertEquals(actualOutput, expectedText);
    }

    @DataProvider
    public Object[][] provide_code_expectedText() {
        return new Object[][] {
                {"echo string literal", "\"test\" amk", "echo \"test\";"},
                {"chained echo string literal", "\"test\" amk \"test2\" amk", "echo \"test\";echo \"test2\";"},
                {"echo expression", "Klammer auf 5 plus 3 Klammer zu zusammengeballt mit \"test\" amk", "echo (5+3).\"test\";"},
                {"sl comment", "da machste nix test", ""},
                {"chained sl comment", "da machste nix test da maschte nix test2", ""},
                {"sl comment after echo", "\"test\" amk da machste nix test", "echo \"test\";"},
                {"echo after sl comment", "da machste nix test \"test\" amk", ""},
                {"sl comment inside string", "\"t1 da machste nix t2\" amk", "echo \"t1 da machste nix t2\";"},
                {"ml comment", "man halt die Fresse test du Hurensohn", ""},
                {"ml comment with echo", "man halt die Fresse \"test\" amk du Hurensohn", ""},
                {"ml comment between echos", "\"test\" amk man halt die Fresse test du Hurensohn \"test\" amk", "echo \"test\";echo \"test\";"},
                {"string assignment", "der testvar ist \"test\"", "$testvar=\"test\";"},
                {"int assignment", "die testvar ist 5", "$testvar=5;"},
                {"float assignment", "das testvar ist 42.12", "$testvar=42.12;"},
                {"var assignment", "die var1 ist der var2", "$var1=$var2;"},
                {"add expression", "die var ist 5 plus 5", "$var=5+5;"},
                {"diff expression", "die var ist 5 minus 5", "$var=5-5;"},
                {"mult expression", "die var ist 5 mal 5", "$var=5*5;"},
                {"div expression", "die var ist 5 durch 5", "$var=5/5;"},
                {"mod expression", "die var ist 5 geteilt mit Rest durch 5", "$var=5%5;"},
                {"string concatenation", "die var ist \"te\" zusammengeballt mit \"st\"", "$var=\"te\".\"st\";"},
                {"equal comparison", "die var ist 5 gleich 5", "$var=5==5;"},
                {"not equal comparison", "die var ist 5 ungleich 5", "$var=5!=5;"},
                {"identical comparison", "die var ist 5 identisch mit 5", "$var=5===5;"},
                {"greater comparison", "die var ist 5 größer als 5", "$var=5>5;"},
                {"smaller comparison", "die var ist 5 kleiner als 5", "$var=5<5;"},
                {"chained expression", "die var ist 5 plus 5 mal 3", "$var=5+5*3;"},
                {"expression with brackets", "die var ist Klammer auf 5 plus 5 Klammer zu mal 3", "$var=(5+5)*3;"},
                {"include string", "\"test.php\" reinballen", "include(\"test.php\");"},
                {"include var", "die var reinballen", "include($var);"},
                {"include expression", "5 zusammengeballt mit \".php\" reinballen", "include(5.\".php\");"},
                {"function", "der Boy will den test am testen sein", "function testen($test){"},
                {"function with multiple params", "der Boy will den test sowie einen test2 am testen sein", "function testen($test,$test2){"},
                {"function without params", "der Boy will am testen sein", "function testen(){"},
                {"curly bracket", "sheesh", "}"},
                {"function with closing bracket", "der Boy will am testen sein sheesh", "function testen(){}"},
                {"function call", "der Boy ist den test am testen", "testen($test);"},
                {"function call without params", "der Boy ist am testen", "testen();"},
                {"function call with multiple params", "der Boy ist den test sowie einen test2 am testen", "testen($test,$test2);"},
                {"function call with expression", "der Boy ist 5 plus 3 am testen", "testen(5+3);"},
                {"function call with multiple expressions", "der Boy ist 5 plus 3 sowie 4 mal 7.5 am testen", "testen(5+3,4*7.5);"},
                {"function call assignment", "die var ist den test am testen", "$var=testen($test);"},
                {"function call assignment with multiple params", "die var ist 5 plus 3 sowie den test am testen", "$var=testen(5+3,$test);"},
                {"function call assignment without params", "die var ist am testen", "$var=testen();"},
                {"assign true", "die var ist real", "$var=true;"},
                {"assign false", "die var ist nicht real", "$var=false;"},
                {"if", "wenn die var real ist", "if($var){"},
                {"if with expression", "wenn 5 plus 3 gleich die var ist", "if(5+3==$var){"},
                {"if with and expression", "wenn die var und die var2 real sind", "if($var&&$var2){"},
                {"else", "ansonsten", "}else{"},
                {"loop", "solange die var real ist", "while($var){"},
                {"array", "die var ist die var2 an der Stelle 4", "$var=$var2[4];"},
                {"array assignment", "die var an der Stelle 2 ist 5 plus 3", "$var[2]=5+3;"},
                {"array with expression", "die var ist die var2 an der Stelle 5 plus 3", "$var=$var2[5+3];"},
                {"array declaration", "die var ist am enlargen", "$var=array();"},
                {"manual $_GET", "die var ist der _GET an der Stelle \"den test\"", "$var=$_GET[\"den test\"];"},
                {"alias", "swagify te_st testen\nder Boy ist am testen", "te_st();"},
                {"return", "die var rausballen", "return $var;"},
        };
    }
}

package de.xinra.boyscript.compiler;

import de.xinra.boyscript.parser.BoyScriptBaseVisitor;
import de.xinra.boyscript.parser.BoyScriptParser;

import java.util.HashMap;

/**
 * Erstellt von Erik Hofer am 12.05.2015.
 */
public class PhpVisitor extends BoyScriptBaseVisitor<String> {
    private HashMap<String, String> alias;

    public PhpVisitor() {
        alias = new HashMap<String, String>();
        alias.put("enlargen", "array");
    }

    @Override
    public String visitAlias(BoyScriptParser.AliasContext ctx) {
        alias.put(ctx.alias.getText(), ctx.original.getText());
        return "";
    }

    @Override
    public String visitComment(BoyScriptParser.CommentContext ctx) {
        return "";
    }

    @Override
    public String visitEcho(BoyScriptParser.EchoContext ctx) {
        return "echo " + visit(ctx.output) + ";";
    }

    @Override
    public String visitFunction(BoyScriptParser.FunctionContext ctx) {
        return "function " + ctx.name.getText() + "(" + (ctx.params == null ? "" : visit(ctx.params)) + "){";
    }

    @Override
    public String visitVariableList(BoyScriptParser.VariableListContext ctx) {
        return visit(ctx.variable()) + (ctx.variableList() == null ? "" : "," + visit(ctx.variableList()));
    }

    @Override
    public String visitExpressionList(BoyScriptParser.ExpressionListContext ctx) {
        return visit(ctx.expression()) + (ctx.expressionList() == null ? "" : "," + visit(ctx.expressionList()));
    }

    @Override
    public String visitCurlyBracket(BoyScriptParser.CurlyBracketContext ctx) {
        return "}";
    }

    @Override
    public String visitInclude(BoyScriptParser.IncludeContext ctx) {
        return "include(" + visit(ctx.file) + ");";
    }

    @Override
    public String visitAssignment(BoyScriptParser.AssignmentContext ctx) {
        visitChildren(ctx);
        return visit(ctx.varName) + "=" + (ctx.expression() == null ? visit(ctx.functionCall()) : visit(ctx.expression())) + ";";
    }

    @Override
    public String visitFunctionCall(BoyScriptParser.FunctionCallContext ctx) {
        String functionName = ctx.functionName.getText();
        if(alias.containsKey(functionName)) {
            functionName = alias.get(functionName);
        }
        return functionName + "(" + (ctx.params == null ? "" : visit(ctx.params)) + ")";
    }

    @Override
    public String visitReturn(BoyScriptParser.ReturnContext ctx) {
        return "return " + visit(ctx.returnValue) + ";";
    }

    @Override
    public String visitFuncCall(BoyScriptParser.FuncCallContext ctx) {
        return visitChildren(ctx)+";";
    }

    @Override
    public String visitCondIf(BoyScriptParser.CondIfContext ctx) {
        return "if(" + visit(ctx.condition()) + "){";
    }

    @Override
    public String visitCondElse(BoyScriptParser.CondElseContext ctx) {
        return "}else{";
    }

    @Override
    public String visitLoop(BoyScriptParser.LoopContext ctx) {
        return "while(" + visit(ctx.condition()) + "){";
    }

    @Override
    public String visitLiteral(BoyScriptParser.LiteralContext ctx) {
        return ctx.getText();
    }

    @Override
    public String visitFalse(BoyScriptParser.FalseContext ctx) {
        return "false";
    }

    @Override
    public String visitTrue(BoyScriptParser.TrueContext ctx) {
        return "true";
    }

    @Override
    public String visitVariable(BoyScriptParser.VariableContext ctx) {
        return "$" + ctx.name.getText() + (ctx.index == null ? "" : "[" + visit(ctx.index) + "]");
    }

    @Override
    public String visitAdd(BoyScriptParser.AddContext ctx) {
        return "+";
    }

    @Override
    public String visitDiff(BoyScriptParser.DiffContext ctx) {
        return "-";
    }

    @Override
    public String visitMult(BoyScriptParser.MultContext ctx) {
        return "*";
    }

    @Override
    public String visitDiv(BoyScriptParser.DivContext ctx) {
        return "/";
    }

    @Override
    public String visitMod(BoyScriptParser.ModContext ctx) {
        return "%";
    }

    @Override
    public String visitConcatenation(BoyScriptParser.ConcatenationContext ctx) {
        return ".";
    }

    @Override
    public String visitEqual(BoyScriptParser.EqualContext ctx) {
        return "==";
    }

    @Override
    public String visitNotEqual(BoyScriptParser.NotEqualContext ctx) {
        return "!=";
    }

    @Override
    public String visitIdentical(BoyScriptParser.IdenticalContext ctx) {
        return "===";
    }

    @Override
    public String visitGreater(BoyScriptParser.GreaterContext ctx) {
        return ">";
    }

    @Override
    public String visitSmaller(BoyScriptParser.SmallerContext ctx) {
        return "<";
    }

    @Override
    public String visitLeftBracket(BoyScriptParser.LeftBracketContext ctx) {
        return "(";
    }

    @Override
    public String visitRightBracket(BoyScriptParser.RightBracketContext ctx) {
        return ")";
    }

    @Override
    public String visitAnd(BoyScriptParser.AndContext ctx) {
        return "&&";
    }

    @Override
    public String visitOr(BoyScriptParser.OrContext ctx) {
        return "||";
    }

    @Override
    public String visitPower(BoyScriptParser.PowerContext ctx) {
        return "**"; //PHP 5 only
    }

    @Override
    protected String aggregateResult(String aggregate, String nextResult) {
        if(aggregate == null) {
            return nextResult;
        } else if(nextResult == null) {
            return aggregate;
        } else {
            return aggregate + nextResult;
        }
    }
}
